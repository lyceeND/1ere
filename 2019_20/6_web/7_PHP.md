% PHP : une rapide découverte


# Du côté serveur de la force

Nous avons travaillé jusqu'à maintenant côté client. Il est temps d'aller du côté
serveur de  la force. Nous avons  envoyé des informations côté  serveur vers une
page dont on a donné l'adresse dans le formulaire `html`:


```html
 <form action="./coteServeur/traitement.php" method="post">
 ```

suite à l'appui sur le bouton `submit`:


```html
<div class="button">
    <button type="submit">Envoyer vos données</button>
</div>
```


Il est temps de les récupérer et de les traiter.


## Créer un serveur

Nous avons  besoin de  créer un  serveur. À  ce stade  de notre  formation, nous
allons en créer un sur notre propre machine, en **local**, pour effectuer nos tests.

Allons dans notre répertoire de travail où se trouve `Formulaire.html`

```console
$ cd le_chemin_vers_le_bon_repertoire/
```

Installons le paquet `php`:

```console
sudo apt-get install php7.2-cli
```

Puis ouvrons un serveur php (option `-S`) sur le port 8000 par exemple:

```console
$ php -S localhost:8000
```

Ouvrons le formulaire à remplir à l'adresse `localhost:8000/Formulaire.html`.
Remplissons-le et appuyons sur le bouton d'envoi.

Une nouvelle page s'ouvre si tout va bien....comment la créer

![php](./IMG/pagephp.png)



# PHP

![php logo](https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Webysther_20160423_-_Elephpant.svg/200px-Webysther_20160423_-_Elephpant.svg.png)


## Un peu d'histoire

[Rasmus    Lerdorf](https://fr.wikipedia.org/wiki/Rasmus_Lerdorf),   programmeur
Groenlandais  a développé  tout  seul à  partir  de  1994 un  langage  en C  lui
permettant  de savoir  si son  site  web était  visité. PHP  voulait dire  alors
*Personnal Home  Page*. Trois  ans plus tard,  deux étudiants  israéliens, [**ZE**ev
Suraski](https://fr.wikipedia.org/wiki/Zeev_Suraski) et [A**ND**i
Gutmans](https://fr.wikipedia.org/wiki/Andi_Gutmans)  ont   créé    le   [Zend
engine](https://en.wikipedia.org/wiki/Zend_Engine),   un    interpréteur   *open
source*  de  PHP  dont  les  initiales  signifient  maintenant  *PHP:  Hypertext
Preprocessor*.

Au  début, PHP  n'était pas  destiné  à être  un langage  de programmation.  Les
versions  primitives  rencontraient   de  nombreux  problèmes  et   ce  qui  est
inquiétant, c'est que plus de la moitié des sites dans le monde tournent sur une
version inférieure à la 5 et la version  stable actuelle est la 7 ce qui effraie
un peu concernant la sécurité de ces sites...

Il y a plus de 300 millions de sites écrits emn PHP. 

Facebook a  lancé en 2014 un  dialecte du PHP,  le [*Hack*](https://en.wikipedia.org/wiki/Hack_(programming_language)) qui propose  un typage
fort. Zend  a régi en  introduisant du typage en  PHP. Nous en  reparlerons plus
précisément en Terminale.



## Présentation du langage

Je vous invite  à parcourir le [tutoriel PHP](https://www.univ-orleans.fr/iut-orleans/informatique/intra/tuto/php/index.html) de  mon collègue Gérard Rozsavolgyi,
co-auteur du Prépabac.



# Traitement des choix de spécialités (`Formulaire.html`)


Nous allons créer une page web qui sera crée dynamiquement grĉe à du code PHP interprété sur le serveur et non pas
chez  le  client  comme  vu  jusqu'à maintenant.  Il  n'y  a  aucune  obligation
d'utiliser PHP mais il y a tellement d'outils proposés et tellement de sites qui
l'utilisent que nous nous devons d'en connaître les principes d'utilisation.


Pour signifier  au serveur que cette  page `html` doit d'abord  être interprétée
par  Zend, on  lui donne  l'extension `.php`.  Nous appelerons  donc notre  page
`traitement.php`   mais   ce  sera   en   fait   une  page   `html`   construite
dynamiquement. Vérifier son code source en ligne : il n'y a pas trace de php.

La  partie de  code `php`  qui sera  interprétée sera  placée entre  des balises
`<?php ... ?>`



## Récupération des données entrées dans le formulaire

Si nous voulons récupérer les données "postées", elles sont dans un *tableau
associatif*, autrement dit un dictionnaire pour les pythonistes:

```php
<?php
//////////// récupération des entrées

$name = htmlspecialchars($_POST['user_name']);
$mail = htmlspecialchars($_POST['user_mail']);
$msg  = htmlspecialchars($_POST['user_message']);
$spes = htmlspecialchars($_POST['spe']);
$or   = htmlspecialchars($_POST['origin']);
```

Les indices du tableau `_POST` sont les `name` introduits dans le formulaire
`html`. Par exemple on avait :

```html
 <input type="text" id="name" name="user_name">
```

Donc `$name` contiendra le nom entré dans le champ identifié par `name`.


La fonction `htmlspecialchars` permet de remplacer tout caractère spécial en son
code `html` ( par exemple `&, ", ', <, >`).


## Stockage des données

Puisque  nous n'étudions  pas  cette année  les bases  de  données, nous  allons
utiliser ce que nous avons vu sur les données en table et créer un fichier `csv`
qui va stocker les données entrées par les utilisateurs.

Nous allons  choisir comme "clé"  d'identification les  adresses e-mail :  on ne
peut  enregister  qu'une  personne  par  e-mail. Il  faut  donc  vérifier  avant
d'ajouter une ligne à notre fichier que l'adresse n'est pas déjà présente.


Pour ajouter  une ligne  en fin de  fichier, nous l'ouvrons  comme em  Python avec
l'option `a` comme `append`:

```php
 /////////////// Écriture de la ligne dans le csv

    $csv = fopen("mes_donnees.csv", "a") or die("Impossible d'ouvrir ce fichier !");

    $champs = $name .','. $mail . ',' .$msg. ',' .$or. ','. $spes[0]. ',' . $spes[1]. "\n";

    if (! $present) {
	fwrite($csv, $champs);
    }
```

`fopen` fonctionne comme le `open` de Python. `die` arrête l'interpréteur.

`$champs` produit une ligne du  `csv`. Vous remarquerez que la **concaténation**
des chaînes en PHP se fait avec le point `.`.

Le `csv` ressemblera donc à ça:

```
zoe,zoe@opp.rr,Vive l'info,alien,sp,ses
annE,anne@opp.rr,Hasta la vista,alien,llce,sp
gil,gil@opp.rr,Baby,alien,svt,llce
Emma,emma@grrr.gr,Au revoir,humain,maths,info
```
 
Il  faut faire  précéder  le code  d'une  vérification de  la  non existence  de
l'adresse e-mail dans le tableau `csv`:

```php
 //////////// Vérification de la présence de l'email dans les données précédentes

    $csv = fopen("mes_donnees.csv", "c+") or die("Impossible d'ouvrir ce fichier !");

    $present = false;
    while(( ! feof($csv)) and (! $present)) {
	$ligne = explode(",", fgets($csv));
	if ($ligne[1] == $mail) {
            $present = true;
            echo "<p class = 'alert'>", $mail, " est déjà présent dans la base </p>\n";
	}
    }
    fclose($csv);
```

L'option `c+` de `fopen` permet de créer  le fichier s'il n'existe pas et le `+`
ajoute le droit de lecture.


On crée un  drapeau (i.e. un booléen  qui nous servira à marquer  la présence de
l'e-mail) `$present`.

La syntaxe du `while ` est standard : `while(condition){action}`.

Le `!` correspond au *non* logique, `feof` renvoie `true` si on a atteint la fin
du fichier.

`fgets(fichier)` permet de lire ligne par ligne un fichier.

`explode(séparateur,  chaîne)` permet  de  créer un  tableau  où chaque  cellule
contient les éléments de la chaîne séparés par...le séparateur.
Donc   ici   `Max,max@bill.fr,Bonjour,alien,info,maths`   devient   un   tableau
contenant les 6 chaînes. En PHP ce ressemble à 
```php
array(
 0=>"Max",
 1=>"max@bill.fr",
 2=>"Bonjour",
 3=>"alien",
 4=>"info",
 5=>"maths"
 )
```

Ainsi `ligne[1]` contient  l'e-mail. On vérifie donc si l'e-mail  est présent et
on sort en marquant le drapeau à `true` si c'est le cas.




Il  va  maintenant   falloir  créer  le  tableau  contenant   les  réponses  des
utilisateurs et pas seulement la dernière réponse. On va donc relire le csv dans
son état actuel et créer un tableau `html` :

```php
 ///////// Lecture du csv et affichage sous forme de tableau

    $csv = fopen("mes_donnees.csv", "r") or die("Unable to open file!");


    $rec = 0;

    echo "<table>\n\n";
    echo "<tr>\n
<th></th>
<th>Nom</th>
<th>E-mail</th>
<th>Message</th>
<th>Origine</th>
<th>Spé 1</th>
<th>Spé 2</th>
</tr>\n";

while (($ligne = fgetcsv($csv)) !== false) {
	echo "<tr>";
	$rec += 1;
	echo "<td>" . $rec . "</td>";
	foreach ($ligne as $cell) {
            echo "<td>" . htmlspecialchars($cell) . " </td>";
	}
	echo "</tr>\n";
}
fclose($csv);
echo "\n</table>\n";
```

On commence à  créer l'en-tête du tableau `html` avec  les balises `th` (cellule
de *head*) et `tr` (*row*):

```php
echo "<table>\n\n";
echo "<tr>\n
	<th></th>
	<th>Nom</th>
	<th>E-mail</th>
	<th>Message</th>
	<th>Origine</th>
	<th>Spé 1</th>
	<th>Spé 2</th>
</tr>\n";
```

Une petite astuce ensuite...[`fgetscsv(fichier
csv)`](https://www.php.net/manual/en/function.fgetcsv.php) 
récupère les lignes d'un  fichier `csv` une par une sous  la forme d'un tableau,
le séparateur par défaut est la virgule.




Quand il n'y a plus de ligne, cette fonction renvoie `false`.

Ainsi on boucle tant qu'on n'a pas atteint la fin du fichier. On affecte en même
temps la variable `$ligne` à cette ligne. 


On commence une ligne:

```php
echo "<tr>";
```

On a  auparavant créé  une varaible  `$rec` qui  compte le  nombre de  lignes du
tableau et on ajoute ce numéro dans la première colonne:

```php
$rec += 1;
echo "<td>" . $rec . "</td>";
```

On boucle ensuite sur les éléments de la ligne qu'on rajoute dans chaque cellule
du tableau successivement:

```php
foreach ($ligne as $cell) {
    echo "<td>" . htmlspecialchars($cell) . " </td>";
}
```

La syntaxe du [`foreach`](https://www.php.net/manual/en/control-structures.foreach.php) est  `foreach(tableau as élément)` qui correspondrait en
Python à `for élément in tableau`.

Ensuite on rajoute  le texte de chaque  cellule de la ligne dans  une cellule du
tableau `html`.

On ferme ensuite la ligne et en sortant du `while` on ferme le tableau:

```php
	echo "</tr>\n";
}
fclose($csv);
echo "\n</table>\n";
```


## Graphique avec JavaScript


Poussons le vice encore plus loin : associons du JS à notre PHP ! Nous voudrions
en effet  créer de beaux graphiques  pour permettre à Mme  Desramé de visualiser
la répartition des Spés.


On               va               pour               cela               utiliser
[`CanvasJS`](https://canvasjs.com/php-charts/animated-chart/)  qui va  permettre
de créer de très jolis graphiques animés.

Prenons par exemple le diagramme en camembert (en "tarte" en anglais : *pie*).

Les  labels  seront les  noms  des  spés et  les  valeurs  seront leur  part  en
pourcentage. 

Il va donc  falloir tout d'abord compter  les spés à chaque  chargement. On crée
pour cela un tableau :


```php
    $specialite = array(
	"info"=>0,
	"maths"=>0,
	"pc"=>0,
	"svt"=>0,
	"llce"=>0,
	"sp"=>0,
	"hlp"=>0,
	"ses"=>0
    );
```

Chaque  valeur   est  initialisée  à   0.   Ensuite,  dans  la   boucle  étudiée
précédemment, on  va ajouter  les valeurs  incrémentées de  chaque spé  pour une
ligne donnée. La spé1 est la 5e cellule et la spé2 la 6e:

```php
while (($ligne = fgetcsv($csv)) !== false) {
	echo "<tr>";
	$rec += 1;
	echo "<td>" . $rec . "</td>";
	foreach ($ligne as $cell) {
            echo "<td>" . htmlspecialchars($cell) . " </td>";
	}
	$specialite[$ligne[4]] += 1;
	$specialite[$ligne[5]] += 1;
```

On  profite  du   fait  que  `specialite`  est  un   tableau  d'association  (un
dictionnaire)  pour appeler  ses valeurs  par  des clés  qui sont  les noms  des
spécialités.


Une fois fini,  on va créer un tableau  de longueur le nombre de  spés où chaque
cellule est un tableau associatif ayant deux clés:
* `label` qui donne le nom de la spé;
* `y` qui donne son effectif.

On part de ce tableau `$specialite`:

```php
Array ( 
[info]  => 5 
[maths] => 5 
[pc]    => 3 
[svt]   => 2 
[llce]  => 4 
[sp]    => 4 
[hlp]   => 1 
[ses]   => 2 
)
```


Alors `$dataspes` a cette allure :

```php
Array (
[0] => Array ( [label] => info  [y] => 5 ) 
[1] => Array ( [label] => maths [y] => 5 ) 
[2] => Array ( [label] => pc    [y] => 3 ) 
[3] => Array ( [label] => svt   [y] => 2 ) 
[4] => Array ( [label] => llce  [y] => 4 ) 
[5] => Array ( [label] => sp    [y] => 4 ) 
[6] => Array ( [label] => hlp   [y] => 1 ) 
[7] => Array ( [label] => ses   [y] => 2 ) 
)
```


`CavasJS` demande comme format de tableau d'entrée une liste de dictionnaires `JSON` de la forme :

```json
[{'label': nom de la spé, 'y': effectif}]
```

On obtient cela avec la fonction PHP [`json_encode`](https://www.php.net/manual/en/function.json-encode)

qui va renvoyer:

```json
[
{"label":"info", "y":5},
{"label":"maths","y":5},
{"label":"pc",   "y":3},
{"label":"svt",  "y":2},
{"label":"llce", "y":4},
{"label":"sp",   "y":4},
{"label":"hlp",  "y":1},
{"label":"ses",  "y":2}
]
```


Regardons maintenant le code JS :

```javascript
let chart_pie = new CanvasJS.Chart('chartContainer', {
      animationEnabled: true,
      exportEnabled: true,
      title:{
             text: 'Spés choisies'
	  },
      subtitles: [{
             text: 'En pourcentages'
      }],
      data: [{
          type: 'pie',
          showInLegend: 'true',
          legendText: '{label}',
          indexLabelFontSize: 16,
          indexLabel: '{label} - #percent%',
          yValueFormatString: '#,##0',
          dataPoints: <?php echo json_encode($dataspes, JSON_NUMERIC_CHECK); ?>
      }]
});
```


On fait de même pour avoir le diagramme en forme de doughnut des origines des inscrits.

Et voilà...


> Regardez le css permettant d'ajuster le style du tableau

> Changez de style de diagramme 

> Rajoutez des champs dans le formulaire : numéro de téléphone, spé abandonnée avec note en e3c, etc. À vous d'imaginer.
