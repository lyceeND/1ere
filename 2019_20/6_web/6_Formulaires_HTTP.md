% Formulaires HTTP

# Remplir un formulaire

On  est sans  cesse  amené à  remplir  des formulaires  en  ligne. C'est  devenu
tellement  courant que  HTML5  a  des balises  spéciales  pour  les traiter.  la
principale commençant par...`<form>` !

Par  exemple, on  voudrait  aider Mme  Desramé à  organiser  les spécialités  de
terminale avec le formulaire suivant à remplir pour les utilisateurs :

![form](./IMG/form.png)

et cette page disponible uniquement pour Mme Desramé:

![php](./IMG/pagephp.png)

* On commence par ouvrir une balise `form`. Nous expliquerons l' option `action`
plus tard. L'option `method=post` nous met  sur la voie: nous allons envoyer sur
un serveur  les résultats.  Nous choisissons  la méthode  POST car  il y  a des
données personnelles.

```html
 <form action="./coteServeur/traitement.php" method="post">
 ```
 
 
* Voyons pour le nom:
 
 ```html
<div>
   <label for="name">Nom :</label>
   <input type="text" id="name" name="user_name">
</div>
 ```

`input=text`  indique que  nous allons  entrer  du texte.  `name` nousservira  à
récupérer les données  côté serveur. `id` peut nous servir  si nous avons besoin
de faire un peu de JS. 


* Pour l'e-mail, c'est presque pareil.  Nous rajoutons un moyen de vérifier que
l'adresse rentrée ressemble à une adresse e-mail:


```html
<div>
    <label for="mail">e-mail :</label>
    <input type="email" id="mail" name="user_mail"
        pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
        placeholder="joe@argh.gl (en minuscules)">
</div>
```

Dans `pattern`,  nous rentrons ce  qu'on appelle une *expression  régulière* qui
vérifie que  l'e-mail est  sous forme  de caractères  minuscules ou  de chiffres
suivis d'un  `@` suivi de  caractères ou de chiffres  suivi d'un point  suivi de
deux lettres.

`placeholder` est un message qui  apparaîtra grisé pour indiquer à l'utilisateur
ce qu'on attend de lui.

* On veut ensuite laisser un peu de place pour que l'utilisateur laisse un
message:

```html
<div>
    <label for="msg">Message :</label>
    <textarea id="msg" rows="10" name="user_message"></textarea>
</div>
```

On laisse 10 lignes. On pourrait rajouter  une option `pattern` pour que l'on ne
rentre pas n'importe quoi.


* On veut  ensuite savoir si l'utilisateur  est humain ou alien.  Ce choix étant
  exclusif, on choisit le type `radio`:
  
```html
<div>
<!-- Bien  choisir le même  "name" pour  que les boutons  soient exclusifs (même groupe)-->
  <p>
	Quel est votre origine :
	<input type="radio" name="origin" value="humain"> 
	<label for="humain">Humain</label>
	<input type="radio" name="origin" value="alien">
	<label for="alien">Alien</label>
  </p>
</div>
```

* Enfin on  veut que l'utilisateur choisisse  ses spés. On peut  lui proposer un
  menu déroulant pour chacune, où il doit sélectionner une des spécialités:
  
```html
<div>
  <p>
    Quelles spés l'an prochain ?<br/>
    <label for="spe1">Spé 1:</label>
    <select id="spe1"  name="spe[]">
      <option value="info">Info</option>
      <option value="maths">Maths</option>
      <option value="pc">Phys Chimie</option>
      <option value="svt">SVT</option>
      <option value="llce">LLCE</option>
      <option value="sp">Sciences Po</option>
      <option value="hlp">Philo Litt</option>
      <option value="ses">SES</option>
    </select>
    <label for="spe2">Spé 2:</label>
    <select id="spe2"  name="spe[]">
      <option value="info">Info</option>
      <option value="maths">Maths</option>
      <option value="pc">Phys Chimie</option>
      <option value="svt">SVT</option>
      <option value="llce">LLCE</option>
      <option value="sp">Sciences Po</option>
      <option value="hlp">Philo Litt</option>
      <option value="ses">SES</option>
    </select>
</div>
```

On remarquera le `name` choisi : le même pour les deux spés et ces `[]` qui vont
nous indiquer que l'on va récupérer côté serveur un tableau.

Il y  a bien d'autres  options. Par  exemple, on aurait  pu choisir des  cases à
cocher pour les spés :

```html
<div>
  <p>
    Quelles spés l'an prochain ?<br/>
    Info<input type="checkbox" name="spe[]" value="info" />
    Maths <input type="checkbox" name="spe[]" value="maths" />
    PC <input type="checkbox" name="spe[]" value="pc" />
    SVT <input type="checkbox" name="spe[]" value="svt" />
    LLCE <input type="checkbox" name="spe[]" value="llce" />
    ScPo <input type="checkbox" name="spe[]" value="sp" />
    HLP <input type="checkbox" name="spe[]" value="hlp" />
    SES <input type="checkbox" name="spe[]" value="ses" />
</div>
```


> Cela a un inconvénient majeur : lequel ?



* Il reste à envoyer toutes ces données. On clique pour cela sur un bouton :

```html
<div class="button">
    <button type="submit">Envoyer vos données</button>
</div>
```



Récupérez maintenant le fichier `Formulaire.html` et jouer avec. 

> Créer un fichier de style pour améliorer l'apparence de cette page.


> Créer  un  script  JS  qui  vérifie  que les  deux  spés  choisies  sont  bien
> différentes.


