% HTTP

# Client-serveur


HTTP (*Hyper  Text Transfer Protocol*) est  le protocole qui régit  les échanges
entre votre appareil actuel,  que ce soit un PC, un mobile,  un iTruc,..., et un
serveur quelque part  dans le monde (et  pourquoi pas dans votre  chambre ou sur
l'appareil que vous utilisez). C'est le modèle **client-serveur** qui correspond
à un schéma question/réponse.



Le client envoie une requête et le serveur lui répond.


Le côté client est souvent appelé *front-en* et le côté serveur *back-end*.



Les navigateurs font le travail de questions/réponses pour nous
habituellement. Utilisons `netcat` pour faire ces requêtes "à la main".





# GET et POST

Installez `netcat` sur votre machine:

```console
$ sudo apt-get install netcat
```

Supposons      que      nous      voulions      accéder      à      la      page
`www.rssweather.com/wx/fr/nantes/wx.php` qui donne la météo de Nantes.

Voici comment y accéder avec `netcat`:

```console
$ nc www.rssweather.com 80
GET /wx/fr/nantes/wx.php http/1.1    
Host: www.rssweather.com
connection: close
```

* On lance `netcat` avec `nc` puis on donne l'**URL** de la page (*Uniform
Ressource Locator*). Cette adresse est décomposée en plusieurs parties:

    * `http` indique le protocole utilisé;
    * `www.rssweather.com` indique le nom du serveur (hôte ou *host*);
    * `80` est le **port** utilisé. Nous verrons plus en détail ce que cela signifie
       lors de l'étude du réseau. Disons que c'est comme pour les bâteaux : cela
       indique où l'on va accoster pour demander et embarquer la marchandise;
    * `/wx/fr/nantes/wx.php` est le chemin vers un fichier.

Il y a des URL bien plus compliquées, d'autres protocoles. Nous verrons cela par
la suite.

On effectue  ensuite notre requête  `GET` qui indique  le fichier que  l'on veut
obtenir en utilisant la version 1.1 du protocole HTTP.

* On précise le nom de l'hôte.

* On appuie deux fois sur `Entrée` et on obtient comme réponse tout d'abord un
en-tête (*header*):



```console
HTTP/1.1 200 OK
Date: Mon, 30 Mar 2020 12:59:35 GMT
Content-Type: text/html
Set-Cookie: __cfduid=d216a8ba18287affb7f2d180df606850d1585573155; expires=Wed, 29-Apr-20 12:59:15 GMT; path=/; domain=.rssweather.com; HttpOnly; SameSite=Lax
Vary: Accept-Encoding
CF-Cache-Status: DYNAMIC
Server: cloudflare
CF-RAY: 57c201bcfdb8dc07-LHR
X-Cache: MISS from lux-02-28-01
X-Cache-Lookup: MISS from lux-02-28-01:0
Via: 1.1 lux-02-28-01 (squid/3.5.20)
Connection: close
```



Puis vient ensuite le corps de la réponse (*body*) qui est une page HTML.


# localhost

Pour tester  notre travail  à partir  de maintenant,  nous pouvons  installer un
serveur sur notre propre machine...Waouh !


Dans un terminal :

```console
python -m http.server 8000
```

que vous laissez de côté puis ouvrez un autre terminal :

```console
$ nc localhost 8000
GET / HTTP/1.1
Host: localhost
Connection: close
```

Vous aurez l'ent-tête:

```console
HTTP/1.0 200 OK
Server: SimpleHTTP/0.6 Python/3.7.4
Date: Mon, 30 Mar 2020 13:35:47 GMT
Content-type: text/html; charset=utf-8
Content-Length: 5944
```

et le corps qui  est enfait le code HTML d'une page donnant  le contenu de votre
`home`. Waouh.

Vous pouvez aussi l'ouvrir dans votre navigateur préféré en tapant dans la barre
d'adresse:

```
localhost:8000/
```



La page HTML que nous avons écrite  dans les sections précédentes est placée ici
dans mon arborsecence:

```
/LYCEE/INFO/1ere/2019_20/6_web/CODE/PremierePage.html
```

Donc voici une copie du terminal pour récupérer son code HTML :


```console
$ nc localhost 8000
GET /LYCEE/INFO/1ere/2019_20/6_web/CODE/PremierePage.html HTTP/1.1
Host: localhost
Connection: close

HTTP/1.0 200 OK
Server: SimpleHTTP/0.6 Python/3.7.4
Date: Mon, 30 Mar 2020 13:56:11 GMT
Content-type: text/html
Content-Length: 830
Last-Modified: Sun, 22 Mar 2020 14:49:52 GMT

<!DOCTYPE html>
<html lang="fr">
  
  <head>
    <title>Page émotion</title>
    <meta charset="utf-8">
    <link href="styles/generique.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <h1>Ma première page web</h1>
    <p>Que d'<em>émotion</em> !</p>
    <p>J'ai envie de:</p>
    <ul>
      <li id="prems" class="important">créer de belles pages</li>
      <li>en écrivant le moins possible</li>
      <li>dans mon hamac</li>
      <li class="important">sous mon arbre</li>
      <li>dans mon jardin</li>
    </ul>

    <p> Pour vous faire rêver pendant le confinement, voici une photo de Ste-Pazanne:</p>
    <p>
      <img src="https://i.ytimg.com/vi/uxmrldXRL_w/hqdefault.jpg" alt="Une belle photo du plus beau village du monde" />
    </p>
  </body>
  <script src="scripts/monalerte.js"></script>
</html>
```

> La méthode GET ne doit être utilisée que pour récupérer des informations.

On peut en effet faire transiter des  données avec la méthode GET via l'URL mais
cela signifie aussi que ces données seront lisibles par tout le monde.

On peut imaginer en effet une requête GET de ce type:


```
http://jean.pierre:mon_mot_de_passe@www.mabanque.fr:80/mon_compte/?tag=retrait&order=1000
```

ou encore:

```
http://www.mabanque.fr/login/?username=jean.pierre&password=mon_mot_de_passe
```

ce qui n'est pas très malin car nous verrons que n'importe qui peut intercepter
cette requête  et décider de  déposer de l'argent  sur votre compte.  C'est vous
rendre vulnérable  à l'attaque  *man in  the middle*  (vous noterez  l'emploi de
*man* et non pas *woman*).



Pour prévenir ce genre d'attaque, on peut utiliser le protocole HTTP**S** qui va
chiffrer  tout ce  qui  suit le  nom  de  l'hôte et  donc  rendre votre  requête
invisible.


Ce protocole utilise  le TLS (*Transport Layer Security*) qui est le  successeur de SSL
(*Secure Sockets Layer*) comme  nous le verrons
plus précisément dans le chapitre sur le réseau.

La plupart des navigateurs utilisent par  défaut HTTPS et mettent en garde quand
un site n'est pas  configuré pour échanger selon ce protocole  (comme le site de
la mairie de Sainte-Pazanne...).

Certain sites ne fonctionnent qu'avec ce protocole comme par exemple `gitlab` ou
`stackoverflow`.

Pour utiliser `netcat`, il faut installer une nouvelle bibliothèque:

```console
$ sudo apt-get iinstall nmap
```

Puis préciser le nouveau port sécurisé qui est normalement 443.

Par exemple pour récupérer notre page de cours:

```console
$ ncat --ssl gitlab.com 443
GET /lyceeND/1ere/-/blob/master/2019_20/6_web/5_HTML.md HTTP/1.1
Host: gitlab.com

HTTP/1.1 200 OK
Date: Mon, 30 Mar 2020 14:37:56 GMT
Content-Type: text/html; charset=utf-8
Transfer-Encoding: chunked
Connection: keep-alive
Set-Cookie: __cfduid=d1e2e28bbf104d185e95da20729c202d51585579076; expires=Wed, 29-Apr-20 14:37:56 GMT; path=/; domain=.gitlab.com; HttpOnly; SameSite=Lax; Secure
Vary: Accept-Encoding
Cache-Control: max-age=0, private, must-revalidate
Content-Security-Policy: connect-src 'self' https://assets.gitlab-static.net https://gl-canary.freetls.fastly.net wss://gitlab.com https://sentry.gitlab.net https://customers.gitlab.com https://snowplow.trx.gitlab.net https://sourcegraph.com https://ec2.ap-east-1.amazonaws.com https://ec2.ap-northeast-1.amazonaws.com https://ec2.ap-northeast-2.amazonaws.com https://ec2.ap-northeast-3.amazonaws.com https://ec2.ap-south-1.amazonaws.com https://ec2.ap-southeast-1.amazonaws.com https://ec2.ap-southeast-2.amazonaws.com https://ec2.ca-central-1.amazonaws.com https://ec2.eu-central-1.amazonaws.com https://ec2.eu-north-1.amazonaws.com https://ec2.eu-west-1.amazonaws.com https://ec2.eu-west-2.amazonaws.com https://ec2.eu-west-3.amazonaws.com https://ec2.me-south-1.amazonaws.com https://ec2.sa-east-1.amazonaws.com https://ec2.us-east-1.amazonaws.com https://ec2.us-east-2.amazonaws.com https://ec2.us-west-1.amazonaws.com https://ec2.us-west-2.amazonaws.com https://iam.amazonaws.com; frame-ancestors 'self'; frame-src 'self' https://www.google.com/recaptcha/ https://www.recaptcha.net/ https://content.googleapis.com https://content-cloudresourcemanager.googleapis.com https://content-compute.googleapis.com https://content-cloudbilling.googleapis.com https://*.codesandbox.io; img-src * data: blob:; object-src 'none'; script-src 'self' 'unsafe-inline' 'unsafe-eval' https://assets.gitlab-static.net https://gl-canary.freetls.fastly.net https://www.google.com/recaptcha/ https://www.gstatic.com/recaptcha/ https://www.recaptcha.net/ https://apis.google.com 'nonce-ZCfK9txvVnANUiUKM9c5PQ=='; style-src 'self' 'unsafe-inline' https://assets.gitlab-static.net https://gl-canary.freetls.fastly.net; worker-src https://assets.gitlab-static.net https://gl-canary.freetls.fastly.net https://gitlab.com blob:
Referrer-Policy: strict-origin-when-cross-origin
Set-Cookie: experimentation_subject_id=ImE3ZmQwOTZlLWNkNWUtNGYyZS05MDdjLTdlMDhkMmQ5NGI1NSI%3D--f2e078575cdc3e959dd287943b886453c843f191; domain=.gitlab.com; path=/; expires=Fri, 30 Mar 2040 14:37:56 -0000; secure; HttpOnly
X-Content-Type-Options: nosniff
X-Download-Options: noopen
X-Frame-Options: DENY
X-Permitted-Cross-Domain-Policies: none
X-Request-Id: WRTutZGVVc3
X-Runtime: 0.194186
X-Ua-Compatible: IE=edge
X-Xss-Protection: 1; mode=block
Strict-Transport-Security: max-age=31536000
Referrer-Policy: strict-origin-when-cross-origin
GitLab-LB: fe-13-lb-gprd
GitLab-SV: web-30-sv-gprd
set-cookie: _gitlab_session=799f382ad8b6e8d15cf88ea9b97a7ab8; path=/; expires=Mon, 30 Mar 2020 16:37:56 -0000; secure; HttpOnly
CF-Cache-Status: DYNAMIC
Expect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"
Server: cloudflare
CF-RAY: 57c292152c42e600-LHR



7ffa
<!DOCTYPE html>
<html class="" lang="en">
<head prefix="og: http://ogp.me/ns#">
<meta charset="utf-8">
<link href="https://assets.gitlab-static.net" rel="dns-prefetch">
<link crossorigin="" href="https://assets.gitlab-static.net" rel="preconnnect">
.
.
.
.

```


> Mais des attaques sont encore possibles.  Donc, pour envoyer des données, il faut
> utiliser la méthode POST qui, comme son nom l'indique, sert à poster des
> informations.

Avec  la  méthode POST,  les  informations  destinées  au  serveur ne  sont  pas
visibles dans l'URL. 

Nous  allons mettre  ceci en  oeuvre  dans les  formulaires HTTP  de la  section
suivante.





