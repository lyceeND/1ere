let canvas = document.querySelector('canvas');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

let c = canvas.getContext('2d');

let rayon = 30;
let nb_cercles = 20;

function rand_signe() {
    const n = Math.random();
    if (n > 0.5) {
        return 1;
    }
    else {
        return -1;
    }
}

function rand(min, max) {
    return min + Math.random() * (max - min);
}

function centre() {
    return {
        x: rand(rayon, innerWidth - rayon),
        y: rand(rayon, innerHeight - rayon),
        dx: rand_signe() * rand(5,10),
        dy: rand_signe() * rand(5,10)
    };
}


function dessine(o) {
    c.beginPath();
    c.arc(o.x, o.y, rayon, 0, Math.PI*2, false);
    c.strokeStyle = 'blue';
    c.stroke();
}

function maj(o) {
    if (o.x + rayon >= innerWidth || o.x - rayon < 0) {
        o.dx = -o.dx;
    }
    if (o.y + rayon >= innerHeight || o.y - rayon < 0) {
        o.dy = -o.dy;
    }
    o.x += o.dx;
    o.y += o.dy;
    dessine(o);
}

let cercles = [];

for (let i = 0; i < nb_cercles; i++) {
    cercles.push(centre());
}

function bouge() {
    requestAnimationFrame(bouge);
    c.clearRect(0, 0, innerWidth, innerHeight);
    for (let o of cercles) {
        maj(o);
    }
}


bouge();