<?php

//////////// récupération des entrées

$name = htmlspecialchars($_POST['user_name']);
$mail = htmlspecialchars($_POST['user_mail']);
$msg  = htmlspecialchars($_POST['user_message']);
$spes = htmlspecialchars($_POST['spe']);
$or   = htmlspecialchars($_POST['origin']);


////////////// Création des tableaux qui récupèreront les données pour les diagrammes 

$origin = array(
    "humain"=>0,
    "alien"=>0
);

$specialite = array(
    "info"=>0,
    "maths"=>0,
    "pc"=>0,
    "svt"=>0,
    "llce"=>0,
    "sp"=>0,
    "hlp"=>0,
    "ses"=>0
);


//////////// Vérification de la présence de l'email dans les données précédentes

$csv = fopen("mes_donnees.csv", "c+") or die("Impossible d'ouvrir ce fichier !");

$present = false;
while(( ! feof($csv)) and (! $present)) {
    $ligne = explode(",", fgets($csv));
    if ($ligne[1] == $mail) {
        $present = true;
        echo "<p class = 'alert'>", $mail, " est déjà présent dans la base </p>\n";
    }
}
fclose($csv);


/////////////// Écriture de la ligne dans le csv

$csv = fopen("mes_donnees.csv", "a") or die("Impossible d'ouvrir ce fichier !");

$champs = $name .','. $mail . ',' .$msg. ',' .$or. ','. $spes[0]. ',' . $spes[1]. "\n";

if (! $present) {
    fwrite($csv, $champs);
}



///////// Lecture du csv et affichage sous forme de tableau

$csv = fopen("mes_donnees.csv", "r") or die("Unable to open file!");


$rec = 0;

echo "<table>\n\n";
echo "<tr>\n
<th></th>
<th>Nom</th>
<th>E-mail</th>
<th>Message</th>
<th>Origine</th>
<th>Spé 1</th>
<th>Spé 2</th>
</tr>\n";

while (($ligne = fgetcsv($csv)) !== false) {
    echo "<tr>";
    $rec += 1;
    echo "<td>" . $rec . "</td>";
    foreach ($ligne as $cell) {
        echo "<td>" . htmlspecialchars($cell) . " </td>";
    }
    $specialite[$ligne[4]] += 1;
    $specialite[$ligne[5]] += 1;
    $origin[$ligne[3]] += 1;
    echo "</tr>\n";
}
fclose($csv);
echo "\n</table>\n";



////////////////  Les array pour les diagrammes
$dataspes = array();
$datacat = array();

foreach($specialite as $spe=>$spe_val)
{
    array_push($dataspes,array("label"=>$spe, "y"=>$spe_val));
}
foreach($origin as $cat=>$cat_val)
{
    array_push($datacat,array("label"=>$cat, "y"=>$cat_val));
}

//////////////////  FIN DU PHP
?>



<!DOCTYPE HTML>
<html>
  <head>
    <link href='style_php.css' rel='stylesheet' type='text/css'>
    <script>
     //// Fonction JS qui fabrique les diagrammes au chargement de la page
     window.onload = function () {

	 ///////// Diagramme en camembert des spés
	 let chart_pie = new CanvasJS.Chart('chartContainer', {
             animationEnabled: true,
             exportEnabled: true,
             title:{
                 text: 'Spés choisies'
             },
             subtitles: [{
                 text: 'En pourcentages'
             }],
             data: [{
                 type: 'pie',
                 showInLegend: 'true',
                 legendText: '{label}',
                 indexLabelFontSize: 16,
                 indexLabel: '{label} - #percent%',
                 yValueFormatString: '#,##0',
                 dataPoints: <?php echo json_encode($dataspes, JSON_NUMERIC_CHECK); ?>
             }]
         });

	 //////////// diagrammes en dooughnut des catégories

	 let chart_doughnut = new CanvasJS.Chart("chartContainer2", {
             animationEnabled: true,
             title:{
                 text: "Categories"
             },
             data: [{
                 type: "doughnut",
                 startAngle: 60,
                 //innerRadius: 60,
                 indexLabelFontSize: 17,
                 indexLabel: "{label} - #percent%",
                 toolTipContent: "<b>{label}:</b> {y} (#percent%)",
                 dataPoints: <?php echo json_encode($datacat, JSON_NUMERIC_CHECK); ?>
             }]
         });
	 //// lancement des tracés
	 chart_pie.render();
	 chart_doughnut.render();
     }
    </script>
</head>

<body>
<br/>
<div id='chartContainer' style='height: 370px; width: 100%;'></div>
<br/>
<div id='chartContainer2' style='height: 370px; width: 100%;'></div>
                       
<script src='https://canvasjs.com/assets/script/canvasjs.min.js'></script>

</body>
</html>

