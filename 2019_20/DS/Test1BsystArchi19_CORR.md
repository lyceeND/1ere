---
date: '7 Novembre 2019'
title: Test d'informatique - 1ère - Sujet B
---
# Arborescence

Voici une arborescence:

![arbo1](Diag2.png)


1. Donnez les  commandes qui permettent de créer cette  arborescence sachant que
   vous  disposez  au   départ  des  répertoires  `/`,   `home`,  `usr`,  `tmp`,
   `mozilla0`, `bin`, `Joe` et `Amy`.
   
```bash
$ cd /home/Joe/
$ mkdir -p ./A/B/F ./A/B/G ./A/C/D ./A/C/E
$ cd ../Amy
$ mkdir -p 1/4 1/5/7 3/6 2/8
```
   
   
2. Créez deux fichiers vides appelés `un` et `deux` dans `bin`.

```bash
$ sudo touch /usr/bin/un
$ sudo touch /usr/bin/deux
```

3. Copiez le fichier `un` dans le répertoire `1` en lui donnant le nom `trois`.

```bash
$ cp /usr/bin/un /home/Amy/1/trois
```

4. Comment réaliser la copie précédente en utilisant un chemin **relatif** si
   vous êtes:
   1. dans le répertoire `A` ?
   
```bash
$ cp ../../../usr/bin/un ../../Amy/1/trois
```
   
   2. dans le répertoire `B` ?
   
```bash
$ cp ../../../../usr/bin/un ../../../Amy/1/trois
```
   
   3. Dans le répertoire `bin` ?
```bash 
$ cp ./un ../../home/Amy/1/trois
```
   
5. Comment renommer le fichier `deux` en `two` ?

```bash
$ sudo mv /usr/bin/deux /usr/bin/two
```

6. On se trouve dans le répertoire `7`. Donnez la commande qui permet d'aller
   dans le répertoire  `6` en suivant un  chemin **relatif**, puis  en suivant un
   chemin **absolu**.
   
```bash
$ cd ../../../3/6

$ cd /home/Amy/3/6
```
   
   
7. On se trouve dans le répertoire `3`. Donnez les commandes qui permettent de
   supprimer le répertoire `mozilla0`.

```bash
$ sudo rmdir ../../../tmp/mozilla0
```

# Droits


Voici des renseigenments sur des fichiers d'un certain répertoire:

```console
amy@elle:~/DS$ ls -l
total 16
-rw-r--r-- 45 ben  print  19  Oct 29 23:48 fichierA
-rwx--x--x 54 bob  adm    95  Oct 29 23:48 fichierB
-rwx------ 10 bart game   25  Oct 29 23:48 fichierC
-r-xr----- 13 bea  staff  118 Oct 29 23:49 fichierD
```

1. Donner, pour chaque fichier:
   1. son nom
   2. le nom de son propriétaire
   3. les droits du propriétaire
   4. les droits du groupe
   5. La taille du fichier
   
```console
fichierA ben  r-- 19o
fichierB bob  --x 95o
fichierC bart --- 25o
fichierD bea  r-- 118o
```
   
2. Donner en octal les droits de chaque fichier.

```console
rw- r-- r-- ⟶ 110 100 100 ⟶ 644
rwx --x --x ⟶ 111 001 001 ⟶ 711
rwx --- --- ⟶ 111 000 000 ⟶ 700
r-x r-- --- ⟶ 101 100 000 ⟶ 540
```

3. Donner le droit de lecture au groupe pour `fichierC`

```console
$ chmod g+r fichierC
```


4. Enlever le droit d'exécution aux autres pour `fichierB`

```console
$ chmod o-x fichierB
```



# Flux, redirections et scripts

1.  Donner une  ligne de  commande qui  permet d'écrire,  dans un  fichier nommé
   `dir.txt`, tous les noms de fichiers présents dans le répertoire supérieur.
   
```console
$ ls .. >> dir.txt
```

   
   
2. Écrire  un script  `bash` qui demande  à l'utilisateur de  rentrer un  nom de
   répertoire et qui écrit les noms  des répertoires présents dans le répertoire supérieur
   dans  un fichier  `rep_dir_name.txt` où  `dir_name` est  en
   fait le nom du répertoire entré par l'utilisateur.
   
```bash
#! /bin/bash

read -p "Quel est le nom du répertoire ?" dir_name

ls .. >> rep_$dir_name.txt

```
   
   
3.  Le script  précédent est  enregistré sous  le nom  `the_dirs.sh`. Comment
	l'utiliser dans un  terminal ? Y a-t-il une manipulation  à faire avant de
	pouvoir l'exécuter ? Laquelle ?

```console
$ chmod a+x ./the_dirs.sh
$ ./the_dirs.sh
```


4.  Donner  ce  qui  est  affiché   après  la  dernière  ligne  de  cette  suite
   d'instructions `bash` en justifiant la réponse:
   
```bash
joe@moi:~$ echo Bonjour >> fic.txt # Écrit Bonjour dans le fichier fic.txt
joe@moi:~$ echo Adieu > fic.txt    # Écrase le fichier fic.txt et écrit dedans Adieu
joe@moi:~$ cat < fic.txt | rev >> fic.txt && cat fic.txt # Adieu est affiché, puis écrit à l'envers, puis ueidA est écrit à la suire de fic.txt et son contenu est affiché
```

On voit donc le contenu de `fic.txt` :

```console
Adieu
ueidA
```

5. Expliquer ce que fait le script suivant:

```bash
#!/bin/bash

    if [ $1 -gt $2 ]; then
       echo "$1 et $2 ou 1 et 2, zatize ze kouèchetionne"
    else
       echo "$1 et $2 c'est pour la vie"
    fi

```

La fonction  attend deux arguments représentés  par `$1` et `$2`.  Si le premier
est strictement  supérieur au second,  le premier  message est affiché,  `$1` et
`$2` étant  remplacés par leur  valeur. Sinon, c'est  le second message  qui est
affiché de la même manière.


# Codage des entiers

1. À quel entier relatif correspond le nombre codé sur un octet `1101 1110` ?

On enlève 1 :

    1101 1110 
            1
    1101 1101
On prend le complément à 1 :

`0010 0010`

qui représente 34 donc le nombre est $`-34`$




2. Quelle la représentation en complément à 2 de 29 sur 8 bits ? Sur 16 bits ?

29 en base 2 sur 8 bits :

`0001 1101`

Complément à 1 :

`1110 0010`

+1 :

`1110 0011`

Et voilà

Sur 16 bits on rajoute 8 `1` :

`1111 1111 1110 0011`





3. Quelle la représentation en complément à 2 de 129 sur 8 bits ? Sur 16 bits ?

129 en base 2 sur 8 bits :

`1000 0001`

Complément à 1 :

`0111 1110`

+1

`0111 1111`

Donc -129 s'écrit +127 sur 8 bits

Sur 16 bits:

`1111 1111 0111 1111`




# Codage des caractères

Voici la table ASCII étendue:


|       | 0   | 1   | 2   | 3   | 4   | 5   | 6   | 7   | 8   | 9   | A   | B   | C   | D   | E   | F   |
|:-----:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| **2** |     | !   | "   | #   | $   | %   | &   | '   | (   | )   | *   | +   | ,   | -   | .   | /   |
| **3** | 0   | 1   | 2   | 3   | 4   | 5   | 6   | 7   | 8   | 9   | :   | ;   | <   | =   | >   | ?   |
| **4** | @   | A   | B   | C   | D   | E   | F   | G   | H   | I   | J   | K   | L   | M   | N   | O   |
| **5** | P   | Q   | R   | S   | T   | U   | V   | W   | X   | Y   | Z   | [   | \   | ]   | ^   | _   |
| **6** | `   | a   | b   | c   | d   | e   | f   | g   | h   | i   | j   | k   | l   | m   | n   | o   |
| **7** | p   | q   | r   | s   | t   | u   | v   | w   | x   | y   | z   | {   |     | }   | ~   |     |
| **8** |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| **9** |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| **A** |     | ¡   | ¢   | £   | ¤   | ¥   | ¦   | §   | ¨   | ©   | ª   | «   | ¬   | -   | ®   | ¯   |
| **B** | °   | ±   | ²   | ³   | ´   | µ   | ¶   | ·   | ¸   | ¹   | º   | »   | ¼   | ½   | ¾   | ¿   |
| **C** | À   | Á   | Â   | Ã   | Ä   | Å   | Æ   | Ç   | È   | É   | Ê   | Ë   | Ì   | Í   | Î   | Ï   |
| **D** | Ð   | Ñ   | Ò   | Ó   | Ô   | Õ   | Ö   | ×   | Ø   | Ù   | Ú   | Û   | Ü   | Ý   | Þ   | ß   |
| **E** | à   | á   | â   | ã   | ä   | å   | æ   | ç   | è   | é   | ê   | ë   | ì   | í   | î   | ï   |
| **F** | ð   | ñ   | ò   | ó   | ô   | õ   | ö   | ÷   | ø   | ù   | ú   | û   | ü   | ý   | þ   | ÿ   |


1. Quelle est le code hexadécimal de `Ñ` en Latin-1 ? En UTF-8 ?


On utilise le tableau : `D1` en Latin-1

En UTF-8, comme le caractère est sur plus de 7 bits, on va le coder sur 2 octets selon la méthode habituelle.

`D1 = 1101 0001 ⟶ 110/0 0011 10/01 0001 ⟶ C391`

2. Coder en UTF-8 puis en Latin-1 `Niñà`

`N ⟶ 4E` en Latin-1 et UTF-8
`i ⟶ 69` en Latin-1 et UTF-8
`ñ ⟶ F1` en Latin-1 et `F1 = 1111 0001 ⟶ 1101 1110 1000 0001 ⟶ DE81` en UTF-8
`à ⟶ E0` en Latin-1 et `E0 = 1110 0000 ⟶ 1101 1100 1000 0000 ⟶ DC80` en UTF-8

3. Décoder `C3 80 C3 89 C3 88 C3 8B C3 87` sachant  que ce texte est codé
   en Latin 1.
   
   `Ã  Ã  Ã  Ã  Ã `
   
   
   
   Faites de même sachant qu'il est codé en UTF8.
   
   Prenons par paquets de 2 octets :
   
   `110 00011 10 000001`, enlevons les préfixes : `0000 1100 0001 ⟶ C1 ⟶ Á`
   
   `110 00011 10 001001`, enlevons les préfixes : `0000 1100 1001 ⟶ C9 ⟶ É`
   
   `110 00011 10 001000`, enlevons les préfixes : `0000 1100 1000 ⟶ C8 ⟶ È`
   
   `110 00011 10 001011`, enlevons les préfixes : `0000 1100 1011 ⟶ CB ⟶ Ë`
   
   `110 00011 10 000111`, enlevons les préfixes : `0000 1100 0111 ⟶ C7 ⟶ Ç`
   
# Opérations bit à bit


1. Complétez le tableau suivant en évaluant les opérations proposées à partir des
octets $`x`$ et $`y`$ fournis:

|Propriété | Signification|
|:---------|:------------:|
| `x` |      `01010001`|
| `y` |      `11010110`|
| `x & y` |  `01010000`|
| `x \| y` |  `11010111`| 
| `x ^ y`|   `10000111`|
| `x ↑ y`|   `10101111`|

2. Quel est le résultat de l'opération `(110110 << 1) & 0xD` ?

`
        110110
  << 1 1101100
     & 0001101
      --------
       0001100
`

`(110110 << 1) & 0xD = 0xC`

