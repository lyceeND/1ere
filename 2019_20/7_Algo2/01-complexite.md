# Complexité

## Approche expérimentale

### Le problème

On dispose d'une liste de N nombres.  Déterminez le nombre de 
  triplets dont la somme est nulle
  
```python
def trois_sommes(xs):
    N = len(xs)
    cpt = 0
    for i in range(N):
        for j in range(i + 1, N):
            for k in range(j + 1, N):
                if xs[i] + xs[j] + xs[k] == 0:
                    cpt += 1
    return cpt
```




On mesure le temps d'exécution:

```python
In [1]: %timeit trois_sommes([randint(-10000,10000) for k in range(100)])
10 loops, best of 3: 27.5 ms per loop

In [2]: %timeit trois_sommes([randint(-10000,10000) for k in range(200)])
1 loops, best of 3: 216 ms per loop

In [3]: %timeit trois_sommes([randint(-10000,10000) for k in range(400)])
1 loops, best of 3: 1.82 s per loop
```


Avec des listes par compréhension?

```python
def trois_sommes_comp(xs):
    n = len(xs)
    return len([(i,j,k) for i in range(n) for j in range(i+1, n) for k in range(j+1,n) if xs[i] + xs[j] + xs[k] == 0 ])  
```

Pas de différence notable :(


```python
In [24]: %timeit trois_sommes_comp([randint(-10000,10000) for k in range(100)])
10 loops, best of 3: 28.1 ms per loop

In [25]: %timeit trois_sommes_comp([randint(-10000,10000) for k in range(200)])
1 loops, best of 3: 222 ms per loop

In [26]: %timeit trois_sommes_comp([randint(-10000,10000) for k in range(400)])
1 loops, best of 3: 1.85 s per loop  
```


Cherchons à mieux comprendre cette évolution:


```python
from time import perf_counter
from math import log2

def temps(xs):
    debut = perf_counter()        # on déclenche le chrono
    trois_sommes(xs)              # on lance le calcul
    return perf_counter() - debut # on arrête le chrono quand c'est fini

# on fabrique une liste contenant les temps de calcul pour des longueurs de 100, 200, 400 et 800
t = [temps(range(100 * 2**k)) for k in range(4)]

# on forme la liste des ratios 
ratio = [t[k + 1] / t[k] for k in range(3)]

# on applique la fonction log2 aux éléments de la liste des ratios
logratio = [log2(r) for r in ratio]

```

Alors:

```python
In [4]: ratio
Out[4]: [7.523860206447286, 9.118789882406599, 8.5098312160934]

In [5]: logratio
Out[5]: [2.940628541715559, 3.133284732580891, 3.128693841844642]  
```

> **Qu'est-ce que le logarithme de base 2 (`log2`) ?**
> Pas besoin de faire spé maths : le logarithme **entier** de base 2 **d'un nombre
> entier** strictement positif, c'est le nombre de chiffres de son écriture en base 2 moins 1.
> Par exemple, 9 s'écrit `1001` et $`\lfloor log_2(9)\rfloor=4-1=3`$. 
> Sur python : `int(log2(9))` renvoie bien `3`.
> Pour les nombres qui se situent entre des puissances de 2, le `log2` n'est pas
> entier et renvoie le nombre vérifiant $`2^{\log_2(n)}=n`$







On voit un motif se dessiner...

Il semble  y avoir  proportionnalité entre  la taille  de la  liste et  le temps
d'exécution. Soit `a` le rapport.

```python
In [6]: temps(range(400))
Out[6]: 4.005484320001415  
```



$`4,00 = a\times 400^3 `$ donc $`a\approx 6,25\times 10^{-8}`$

Donc   pour   $`N=1000`$,    on   devrait   avoir   un    temps   de   $`6,25\times
10^{-8}\times10^9=62,5`$


```python
In [7]: temps(range(1000))
Out[7]: 68.54615448799996  
```

### En langage C

Avec ce petit programme:

```c
#include <stdio.h>
#include <time.h>


typedef int Liste[13000];

int trois_sommes(int N)
{
  int cpt = 0;
  Liste liste;

  for ( int k = 0; k < N; k++)
    {
      liste[k] = k - (N / 2);
    }
  
  for (int i = 0; i < N; i++)
    {
      int xi = liste[i];
      for (int j = i + 1; j < N; j++)
	{
	  int sij = xi + liste[j];
	  for (int k = j + 1; k < N; k++)
	    {
	      if (sij + liste[k] == 0) {cpt++;}
	    }
	}
     }
printf("%d\n",cpt);
  return cpt;
}


void chrono(int N)
{
  clock_t temps_initial, temps_final;
  float temps_cpu;

  temps_initial = clock();
  trois_sommes(N);
  temps_final   = clock();
  temps_cpu = ((double) temps_final - temps_initial) / CLOCKS_PER_SEC;
  printf("Temps en sec pour %d : %f\n",N, temps_cpu);

}
  


int main(void)
{
  chrono(100);
  chrono(200);
  chrono(400);
  chrono(800);
  chrono(1600);
  chrono(3200);
  chrono(6400);
  // chrono(12800);

  return 1;
}
```

on obtient:

```bash
$ gcc  -std=c99 -Wall -Wextra  -Werror -pedantic -O4 -o somm3 Trois_Sommes.c
$ ./somm3 
Temps en sec pour 100 : 0.00000
Temps en sec pour 200 : 0.000000
Temps en sec pour 400 : 0.020000
Temps en sec pour 800 : 0.090000
Temps en sec pour 1600 : 0.720000
Temps en sec pour 3200 : 5.760000
Temps en sec pour 6400 : 45.619999
Temps en sec pour 12800 : 360.839996
```



On a la même évolution en $`N^3`$ avec  un rapport de 8 entre chaque doublement de
taille  mais la constante est bien meilleure:

$`45,61 = a\times 6400^3`$ d'où $`a\approx 1,74\times10^{-10}`$

$`360,84 = a\times 12800^3`$ d'où $`a\approx 1.72\times 10^{-10}`$


Le compilateur  C est  en fait intelligent  et évite de  recalculer des
valeurs déjà connues.

Aidons le pauvre `python` à faire de même:


```python
def trois_sommes(xs):
    N = len(xs)
    cpt = 0
    for i in range(N):
        xi = xs[i]
        for j in range(i + 1, N):
            sij = xi + xs[j]
            for k in range(j + 1, N):
                if sij + xs[k] == 0:
                    cpt += 1
    return cpt  
```

On gagne un peu de temps:

```python
In [28]:  xs = list(range(-50,51))
In [29]: %timeit trois_sommes(xs)
100 loops, best of 3: 12.9 ms per loop

In [30]:  xs = list(range(-100,101))
In [31]: %timeit trois_sommes(xs)
10 loops, best of 3: 94.4 ms per loop

In [32]:  xs = list(range(-200,201))
In [33]: %timeit trois_sommes(xs)
1 loops, best of 3: 851 ms per loop

In [34]:  xs = list(range(-400,401))
In [35]: %timeit trois_sommes(xs)
1 loops, best of 3: 7.04 s per loop  
```

mais la progression semble toujours la même.

### Loi de Brooks

*Adding manpower to a late software project  makes it later a result of the
fact that  the expected  advantage from  splitting work  among N  programmers is
$`O(N)`$, but the complexity and  communications cost associated with coordinating
and then merging their work is $`O(N^2)`$*

## Grand O

On n'entrera pas dans les détails mathématiques. Les informaticiens ont intégré
dans  leur  jargon  geek  le  *grand O*.   Par  exemple,  avoir  une  complexité
temporelle  en $`O(n^3)`$  c'est  en gros  avoir un  temps  d'exécution qui  est
multiplié par 8 quand on double la taille de l'entrée.


## Complexité en temps / Complexité en mémoire

On distingue deux types de complexité :  celle en temps (comment évolue le temps
d'exécution ?) et celle en mémoire (comment évolue la taille mémoire occupée ?).

Au lycée on s'occupera principalement de la complexité temporelle.


>  **À  retenir** La  complexité temporelle  donne une  idée de  la **VITESSE**
>    d'exécution,  pas du  temps  d'exécution.  D'ailleurs  on
>    s'occupe d'algorithmes et non pas de programmes, donc d'entités qui ne sont
>    pas implémentées sur une machine : juste des méthodes de calcul.

