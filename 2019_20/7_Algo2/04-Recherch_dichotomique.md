# Recherche dichotomique : couper les cheveux...en 2


## Rappel sur la recherche séquentielle

En Grec ancien, διχοτομία signifie **division en deux parties**. En Anglais, on
retrouve  différents termes  :  *binary search*,  *binary chop*,  *half-interval
search*, *logaritmic search*.  C'est plus parlant si vous ne  parlez pas le Grec
ancien couramment (c'est regrettable mais ça existe).

Avec  une recherche  *séquentielle*,  on balaie  la liste  depuis  le début,  on
compare chaque élément  lu à lélément cherché  et on continue tant  qu'on ne l'a
pas trouvé. **Que se passe-t-il alors dans le pire des cas ?**

## Principe de la recherche dichotomique

En voici le principe, avec une liste triée dans l'ordre croissant :

* Si la liste est vide : répondre négativement, la recherche est finie.
* Sinon, trouver la valeur la plus centrale de la liste et comparer cette valeur à l'élément recherché.
    - si la valeur est celle cherchée : répondre positivement, la recherche est finie ;
    - si la valeur est strictement plus petite que l'élément recherché, reprendre la procédure avec la seconde moitié de la liste ;
    - sinon reprendre la procédure avec la première moitié de la liste.



Voici un exemple de mise en œuvre


```python
def recherche_dicho(liste, element):
    """
    liste doit être triée dans l'ordre croissant
    Renvoie vrai si element est dans la liste, False sinon
    """
    indice_debut = 0
    indice_fin = len(liste) - 1
    while indice_debut <= indice_fin:
        indice_centre = (indice_debut + indice_fin) // 2
        valeur_centrale = liste[indice_centre]
        if valeur_centrale == element:
            return True
        elif valeur_centrale < element:
            indice_debut = indice_centre + 1
        else:
            indice_fin = indice_centre - 1
    return False
```




## Une expérience 



![Lancer d'actionnaires](./IMG/hudsucker.JPG)


* *k* actionnaires
* $`N=2^n`$ étages
* RDC  = 0
* Il existe un étage fatal
* RDC non fatal
* Minimiser le nombre d'essais



*  Première idée:  on commence  au  rez-de-chaussée et  on progresse  d'un
    étage.
* Combien d'essais au pire?
* En moyenne ?



```shell
FONCTIOΝ ChercherEntre(N: entier) -> entier
# pré-condition.: il existe un étage fatal entre inf non compris et sup, inf < sup
# invariant à l'intérieur de la boucle: le  plus petit étage fatal est entre inf
# (non compris) et sup
# variant à l'intérieur de la boucle : la taille de l'intervalle des étages candidats
# post-condition: la valeur retournée est le plus petit étage fatal
inf,sup ⟵ 0, N
tant que sup > inf + 1 faire
   # plus petit étage fatal est entre inf non compris et sup, sup > inf + 1
   milieu ⟵ ( inf + sup ) // 2
   si estFatal(milieu)
      sup = milieu
   sinon 
      inf = milieu
   fin_si
fin_tant_que
#le plus petit étage fatal est entre inf (non compris) et sup. De plus sup = inf + 1
retourne sup
# la valeur retournée est le plus petit étage fatal
```




#### QUESTIONS

* étude de la *terminaison* de l'algorithme: est-ce que la fonction
  renvoie effectivement une valeur?
* étude  la  *correction*  de l'algorithme: est-ce que  la fonction
  renvoie la valeur attendue?
*  étude  de la   *complexité*  de  l'algorithme: peut-on  estimer  la
  vitesse d'exécution de cet algorithme?
  

#### RÉPONSES

* **TERMINAISON**:  $`\ell_i=\frac{N}{2^i}=\frac{2^n}{2^i}=2^{n-i} \quad \ell_n=1`$
* **CORRECTION**: l'invariant est vérifié à chaque itération
* **COMPLEXITÉ**:  il suffit donc de compter combien de lancers ont été effectués. La réponse est
  dans l'étude faite  pour prouver la terminaison: c'est à  l'étape *n* que l'on
  atteint la condition de sortie de l'algorithme.
  Que vaut *n*?  
  On a posé au départ que le nombre d'étages était une puissance
  de 2: $`N=2^n`$. Ainsi, $`n=\log_2N`$.



## Et la recherche dichotomique d'une solution d'une équation réelle?

 Cherchons une approximation de $`x^2-2=0`$ par la méthode de dichotomie avec
  une précision de $`2^{-10}`$ entre 1 et 2.  Il va falloir chercher:


* un nombre  (un étage)
* dans un tableau (un immeuble)
* de $`2^{10}`$ nombres (étages)

|$`1`$ | $`1+2^{-10}`$ | $`1+2\times 2^{-10}`$| $`1+3\times 2^{-10}`$|$`\cdots`$|$`1+2^{10}\times 2^{-10}`$|
|------|----------------|------------------------|-----------------------|-----------|----------------------------|






Notre  fonction  booléenne `estFatal`  est alors  le  test  $`x  \mapsto
\mathtt{x\times x <= 2}`$ et l'on va chercher une cellule de ce tableau par dichotomie
comme on cherchait un étage dans un immeuble.


```python
def racineDicho(prec: float) -> Tuple(float, int):
    cpt: int = 0
    inf: float = 1
    sup: float = 2
    while (sup - inf > prec):
        m = inf + (sup - inf) / 2
        cpt += 1
        if m*m <= 2:
            inf = m
        else:
            sup = m
    return sup,cpt
```
  
  
  ```console
In [1]: racineDicho(2**(-10))
Out[1]: (1.4150390625, 10)

In [2]: racineDicho(2**(-15))
Out[2]: (1.414215087890625, 15)

In [3]: racineDicho(2**(-20))
Out[3]: (1.4142141342163086, 20)

In [4]: racineDicho(2**(-30))
Out[4]: (1.4142135623842478, 30)

In [5]: racineDicho(2**(-50))
Out[5]: (1.4142135623730958, 50)
  ```
  
