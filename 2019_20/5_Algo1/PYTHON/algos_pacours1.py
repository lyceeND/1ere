"""
TP 1 : révision sur les algos de parcours : max, somme, moyenne, max, comptage
On suppose qu'on dispose d'un ensemble ou d'une liste de couples (élève, taille)
On n'utilisera que les fonctions arithmétiques basiques mais ni len ni sum 
"""
from math import inf


ma_classe = [
    ('Amybeth', 165),
    ('Gilbert', 178),
    ('AnnE', 165),
    ('Matthew', 175),
    ('Marilla', 170),
    ('Jerry', 185),
    ('Rachel', 150)
    ]


#1 Nombre d'élèves
def nb_eleves(classe: list) -> int:
    compteur = 0
    for eleve in classe:
        compteur += 1
    return compteur

assert nb_eleves([]) == 0, "nb_eleves bugue avec  liste vide"
assert nb_eleves(ma_classe) == 7, "nb_eleve compte mal"

#2 Moyenne des tailles
def moyenne_taille(classe: list) -> int:
    assert classe != [], "une classe vide n,a pas de moyenne"
    # on aurait pu aussi choisir de renvoyer None par exemple
    compteur = 0
    som_taille = 0
    for eleve in classe: # ou bien for nom, taille in classe:
        compteur += 1
        som_taille += eleve[1] # ou bien som_taille += taille
    return som_taille // compteur

assert moyenne_taille([('a',10), ('b',15)]) == 12, "Pb dans calcul moyenne :( "

#3 nom du plus grand
def plus_grand(classe: list) -> str:
    assert classe != [], "pas de plus grd dans liste vide !"
    geant_provisoire = classe[0]
    for eleve in classe:
        if eleve[1] > geant_provisoire[1]:
            geant_provisoire = eleve
    return geant_provisoire[0]

assert plus_grand(ma_classe) == "Jerry", "pb dans plus _grand"

#4 nombre d'élèves dont la taille est à moins de 2 cm de la taille moyenne
def ecart_moy(classe: list) -> int:
    compteur = 0
    moy = moyenne_taille(classe)
    for eleve in classe:
        if moy - 2 <= eleve[1] <= moy + 2: #filtre
            compteur += 1
    return compteur

#5 nombre d'élèves dont le prénom commence par une voyelle 
def nb_eleves_voy(classe:list) -> int:
    voy:str = 'AEIOUY'
    nb_voy:int = 0
    for eleve in classe:
        if eleve[0][0].upper() in voy:
            nb_voy += 1
    return nb_voy


#6 Nom des élèves dont le nom commence par une consonne et mesurant plus que la taille moyenne
def eleves_consonne(classe: list) -> list:
    liste_filtree = []
    moy = moyenne_taille(classe)
    voy = 'aeiouy'
    for nom, taille in classe:
        if nom[0].lower() not in voy and taille > moy:
            liste_filtree.append(nom)
    return liste_filtree

def eleves_consonne_v2(classe: list) -> list:
    moy = moyenne_taille(classe)
    voy = 'aeiouy'
    return [nom for nom, taille in classe if nom[0].lower() not in voy and taille > moy]

#7 Calculer la moyenne olympique des tailles (i.e. en enlevant les 2 extrêmes)
def olympic(classe: list) -> list:
    taille_mini: int = inf
    taille_maxi: int = -inf 
    total_tailles: int = 0
    nb_el: int = 0
    for nom, taille in classe:
        total_taille += taille
        nb_el += 1
        if taille < taille_mini:
            taille_mini = taille
        if taille > taille_maxi:
            taille_maxi = taille
    return (total_tailles - taille_mini - taille_maxi) // (nb_el - 2)


"""
TP 2 : tri
"""

# Trier les élèves selon leur taille

# Trier les élèves par ordre alphabétique

# Trier les élèves par rapport à la longueur de leur nom 
 
# Trier les élèves par rapport à leur distance à la taille moyenne