"""
TP 2 : tri
"""

import random, time
import matplotlib.pyplot as plt

def liste(n:int) -> list:
    """
    retourne une liste des entiers de 0 à n-1
    dans un ordre quelconque
    """
    return random.sample(list(range(n)),n)

def tri_bulle(xs:list) -> None:
    chgt = True
    n = len(xs)
    while chgt:
        chgt = False
        for k in range(n - 1):
            if xs[k] > xs[k + 1]:
                xs[k], xs[k +1] = xs[k + 1], xs[k]
                chgt = True

def mesure_tps(tri, n:int) -> float:
    """
    Renvoie le temps nécessaire au tri d'une liste de longueur n
    avec le tri donné en argument
    """
    xs = liste(n)
    a = time.time()
    tri(xs)
    b = time.time()
    return b - a

def trace(tri):
    """
    Trace l'évolution du temps de tri de listes de longueurs 0 à 10000
    en utilisant le tri donné en argument
    """
    X = [400*k for k in range(25)]
    Y = [mesure_tps(tri, n) for n in X]
    plt.plot(X,Y)
    plt.show()

#trace(tri_bulle)