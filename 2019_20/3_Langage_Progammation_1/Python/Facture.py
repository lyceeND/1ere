#1 Les importations
from typing import List

#2 Fonctions
def cout(prix: List[float], quants: List[int]) -> float:
    return sum([prix[i]*quants[i] for i in range(len(prix))])


#3 Programme principal
## Entrées
ings = eval(input("Liste des ingrédients : "))
prix = eval(input("Liste des prix : "))
quants = eval(input("Liste des quantités : "))
##Traitement
total = cout(prix, quants)
##Sortie
print(total)
