from mon_csv import depuis_csv, vers_csv
from copy import deepcopy

Groupe1 = depuis_csv('./Groupe1.csv')

Rose = {'Nom':'Rose', 'Anglais':'19', 'Maths':'17','Info':'18'}

Groupe1.append(Rose)

vers_csv(Groupe1, 'newGroupe1', ordre_cols=['Nom', 'Info', 'Anglais','Maths'])

def ajoute_moyenne(table_notes):
    new_table = deepcopy(table_notes)
    matieres = [cle for cle in table_notes[0].keys() if cle != 'Nom']
    nb_notes = len(matieres)
    for eleve in new_table :
        som = 0
        for matiere in matieres :
            som += eval(eleve[matiere])
        eleve['Moyenne'] = str(som/nb_notes)
    return new_table

print(ajoute_moyenne(Groupe1))

